ArrayList<Mouse> mice;

void setup() {
  size(400,400);
  colorMode(HSB);
  fill(200,200,200);
  
  mice = new ArrayList<Mouse>();
}

void draw() {
  background(20);
  
  pushStyle(); // this is for the rect, not for the mice
   fill(0, 30);
   noStroke();
   for(int i=0; i<mice.size(); i++) {
     rect(0, 0, width, height);
     mice.get(i).draw();
   }
  popStyle();
}

void mousePressed() {
  mice.add(new Mouse(new PVector(random(width), random(height)), // position
                     random(2*PI), // rotation
                     random(500))); // size
}
