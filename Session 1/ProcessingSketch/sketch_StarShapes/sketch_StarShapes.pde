StarShape star = new StarShape(50);

void setup() {
  size(500, 500);
  fill(20,180, 230);
  noStroke();
}

void draw() {
  background(10, 20, 100);
  
  star.update();
  star.draw(width/2, height/2, width/2, 5);
  //ellipse(width/2, height/2, width/2, width/2);
}