void setup() {
  size(500, 500);
  smooth(4);
  
 stroke(150);
 strokeWeight(5);
 fill(50, 200, 255);
}

void draw() {
 background(0, 10, 80);
 
 // The point at the middle of the window
 PVector middle = new PVector(width/2, height/2);
 
 int numberOfCircles = 10;
 // This will be the vector we rotate for every circle we draw
 PVector v = new PVector(0, 130);
 float step = 2*PI/numberOfCircles;
 for(int i=0; i<10; i++)
   {
    v.rotate(step); // change the variable v
    PVector pos = PVector.add(middle, v);
    ellipse(pos.x, pos.y, 60, 60);
   }
}