ArrayList<PVector> points;

void setup() {
  size(300,300);
  colorMode(HSB);
  
  stroke(140, 200, 255);
  strokeWeight(3);
  
  points = new ArrayList<PVector>();
}

void draw() {
  background(70);
  
  for(int i=0; i<points.size()-1; i++)
    line(points.get(i).x, points.get(i).y, points.get(i+1).x, points.get(i+1).y);
    
  // if you never press the mouse to clear the points, this list becomes huge and will eventually crash the program!
  points.add(new PVector(random(width), random(height))); 
}

void mousePressed(){
  points.clear();
}