class FollowStar extends FollowShape {
  
  int nPoints;
  
  FollowStar(PVector _position, PVector _size, float _followSpeed, int _nPoints) {
   super(_position, _size, _followSpeed);
   nPoints = _nPoints;
  }
  
  void draw(){
  float angle = TWO_PI / nPoints / 2.0;
  
  // This is the first point that we draw (but normalized)
  // We will rotate this vector to get the other poitns we want to draw
  PVector point = new PVector(1, 0);
  
  pushMatrix();
   translate(getPosition().x, getPosition().y);
   beginShape();
    for (int i=0; i<nPoints; i++) {
       vertex(0.5 * size.x * point.x,  // 0.5*size.x is the outer radius of the star in x
              0.5 * size.y * point.y);
       point.rotate(angle);
       vertex(0.25 * size.x * point.x, // 0.5*size.x is the inner radius of the star in x
              0.25 * size.y * point.y);
       point.rotate(angle);
    }
   endShape(CLOSE);
  popMatrix();
  }
}