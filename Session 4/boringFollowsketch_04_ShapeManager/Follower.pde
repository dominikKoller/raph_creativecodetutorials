class Follower {
   
   PVector position;
   PVector goal;
   float followSpeed;
   
   Follower(PVector _position, PVector _goal, float _followSpeed) {
     position = _position;
     goal = _goal;
     followSpeed = _followSpeed;
   }
   
   Follower(PVector _position, float followSpeed) {
     this(_position, _position, followSpeed);
   }
   
   void update() {
     position = PVector.lerp(position, goal, followSpeed);
   }
}