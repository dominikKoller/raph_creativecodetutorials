void setup() {
  size(500, 500);
  smooth(4);
  
 stroke(150);
 strokeWeight(5);
 fill(50, 200, 255);
}

void draw() {
 background(0, 10, 80);
 
 // The point at the middle of the window
 PVector middle = new PVector(width/2, height/2);
 
 int numberOfTriangles= 10;
 // This will be the vector we rotate for every triangle we draw
 PVector v = new PVector(0, 130);
 float step = 2*PI/numberOfTriangles;
 
 for(int i=0; i<10; i++)
   {
    // the triangle, not yet in the right position
    PVector point1 = new PVector(0, -50);
    PVector point2 = new PVector(-30, 0);
    PVector point3 = new PVector(30, 0);
    
    v.rotate(step); // change the variable v
    PVector pos = PVector.add(middle, v); // the position we want the triangle to be at
    
    // translating all three points of the triangle to the right position
    point1.add(pos);
    point2.add(pos);
    point3.add(pos);
    
    // drawing the triangle
    triangle( point1.x, point1.y,
              point2.x, point2.y,
              point3.x, point3.y);
   }
}