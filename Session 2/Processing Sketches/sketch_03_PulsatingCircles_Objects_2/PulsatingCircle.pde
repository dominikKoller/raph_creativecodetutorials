class PulsatingCircle {
  
 float size;
 PVector position;
 color couleur;
 float speed;
 
 public PulsatingCircle(float _size, PVector _position, color _couleur, float _speed){
  size = _size;
  position = _position;
  couleur = _couleur;
  speed = _speed;
 }
 
 public void draw(){
  float currentSize = sin(speed*frameCount / 60) * size;
  fill(couleur);
  ellipse(position.x,
          position.y, 
          currentSize, 
          currentSize); 
 }
}