class Ball {
 PVector speed;
 PVector position;
 float size;
 
 Ball (PVector _speed, PVector _position, float _size) {
   speed = _speed;
   position = _position;
   size = _size;
 }
 
 void applyForce(PVector force){
   speed.add(force);
 }
 
 void update() {
  position.add(speed);
 }
 
 void draw() {
  pushMatrix();
   translate(position.x, position.y);
   ellipse(0,0,size, size);
  popMatrix();
 }
}