ArrayList<Rect> rects;

void setup() {
  size(500, 300);
  rectMode(CENTER);
  colorMode(HSB);
  strokeWeight(5);
  
  rects = new ArrayList<Rect>();

}

void draw() {
  background(50);
  
  // If the mouse is pressed, we'll use a random fill color
  // This will effect the rect, but not the colRect!
  // Since the colRect changes to its own color in draw()
    
  // Convenient new syntax to loop through every element in a collection:
  for(Rect r : rects)
    r.draw(); // They are all Rects! Some are ColoredRects though.
}

void mousePressed(){
  if(mouseButton == LEFT) {
    rects.add(  new Rect(new PVector(mouseX, mouseY),  // position
                         new PVector(40, 40) ));       // size
  }
  
  else if (mouseButton == RIGHT) {
    rects.add(  new ColoredRect(new PVector(mouseX, mouseY),      // position
                                new PVector(40, 40),              // size 
                                color(random(255), 200, 200),     // fill Color
                                color(random(255), 200, 200)));   // stroke Color
  }
}