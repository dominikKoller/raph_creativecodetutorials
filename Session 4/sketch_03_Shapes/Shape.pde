abstract class Shape {
  PVector position;
  PVector size;
  
  Shape(PVector _position, PVector _size) {
    position = _position;
    size = _size;
  }
  
  abstract void draw();
}