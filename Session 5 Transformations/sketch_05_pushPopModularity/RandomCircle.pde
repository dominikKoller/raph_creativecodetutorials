class RandomCircle {
 PVector position;
 float size;
 color coleur;
 
 RandomCircle() {
  position = new PVector(random(width), random(height));
  size = random(100);
  coleur = color(random(255), 200, 255);
 }
 
 void draw(){
  fill(coleur);
  ellipse(position.x, 
          position.y, 
          size, size);  
 }
}
