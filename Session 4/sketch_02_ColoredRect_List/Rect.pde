class Rect {
  PVector position;
  PVector size;
  
  Rect(PVector _position, PVector _size) {
   position = _position;
   size = _size;
  }
  
  void draw(){
   rect(position.x, position.y, size.x, size.y); 
  }
}