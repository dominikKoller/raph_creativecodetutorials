// Let's now look at the order of transforms.

// It makes a difference whether we
// rotate    first and then translate
// or
// translate first and then rotate!

void setup() {
  size(400,400);
  colorMode(HSB);
  noStroke();
  fill(200,200,200);
}

void draw() {
  background(20);
  
  pushMatrix();
   rotate(mouseX / 50.0);
   translate(100, 100);
   fill(200, 200, 200);
   rect(0,0, 100, 100);
  popMatrix();
  
  pushMatrix();
   translate(100, 100);
   rotate(mouseX / 50.0);
   fill(100, 200, 200);
   rect(0,0, 100, 100);
  popMatrix();
}
