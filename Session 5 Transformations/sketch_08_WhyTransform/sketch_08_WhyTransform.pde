// Now: why would we ever want to do this?

// Reason 1: if you draw more complicated things,
// you can easily draw all of them in one spot

void setup() {
  size(400,400);
  colorMode(HSB);
  fill(200,200,200);
}

void draw() {
  background(20);
  
  // Think about how you could translate this manually,
  // and how difficult that would be!
  translate(200,200);
  
  // amazingly drawn mouse:
  noStroke();
  fill(100, 240, 255);
  ellipse(40, -40, 80, 80);
  ellipse(-40, -40, 80, 80);
  
  fill(150, 200, 200);
  ellipse(0,0, 100, 100);
  
  fill(100, 10, 255);
  ellipse(15, -20, 35, 35);
  ellipse(-15, -20, 35, 35);
  
  fill(245, 200, 255);
  ellipse(0, -5, 20, 20);
  
  translate(200, 100);
}
