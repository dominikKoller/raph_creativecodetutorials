class StarShape {
  float FUpdateSpeed = 0.005; // how quickly to lerp to randomized positions
  float FRandomizeEvery = 100; // randomize positions every 100 frames
  
  
  int FSpikeCount;
  int FRandomizeCounter = 0;
  PVector[] FVertices;
  PVector[] FVerticesGoTo;
  
  StarShape(int spikeCount) {
    FSpikeCount = spikeCount;
    FVerticesGoTo = new PVector[FSpikeCount];
    FVertices = new PVector[FSpikeCount];
    
    float rotationStep = 2*PI / FSpikeCount;
    for(int i=0; i<FSpikeCount; i++) {
      FVerticesGoTo[i] = PVector.fromAngle(rotationStep*i);
      FVertices[i] = FVerticesGoTo[i].copy();
    }
    
    initRandomDistances();
  }
  
  public void draw(float x, float y, float size, float power) {   
    
    beginShape();
    for(int i=0; i<FSpikeCount; i++)
    {
      PVector newVertex = FVertices[i].copy();
      newVertex.setMag(pow(newVertex.mag(), power) * size/2.0);
      vertex(x + newVertex.x, y + newVertex.y);
    }
    endShape(CLOSE);
  }
  
  public void update() {
    for(int i=0; i<FSpikeCount; i++)
      FVertices[i].lerp(FVerticesGoTo[i], FUpdateSpeed);
      
    if(FRandomizeCounter%FRandomizeEvery == 0)
      initRandomDistances();
    FRandomizeCounter++;
  }
    
  private void initRandomDistances() {
  float randomIntensity = 0.25; // arbitrarily chosen, should be >0 and <1
  
  for(int i=0; i<FSpikeCount; i++)
      FVerticesGoTo[i].setMag(1.0 + random(randomIntensity) - randomIntensity/2.0);
  }
}