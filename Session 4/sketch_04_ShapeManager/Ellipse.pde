class Ellipse extends Shape {
  
  Ellipse(PVector position, PVector size, int lifeTime, color c) {
   super(position, size, lifeTime, c);
  }
  
  void draw(){
   pushStyle();
    fill(c);
    ellipse(position.x, position.y, size.x, size.y); 
   popStyle();
  }
}