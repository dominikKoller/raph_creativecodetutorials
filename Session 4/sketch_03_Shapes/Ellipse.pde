class Ellipse extends Shape {
  
  Ellipse(PVector _position, PVector _size) {
   super(_position, _size);
  }
  
  void draw(){
   ellipse(position.x, position.y, size.x, size.y); 
  }
}