void setup() {
 size(500, 500);
 smooth(4);
  
 stroke(150);
 strokeWeight(5);
 fill(50, 200, 255);
 
 rectMode(CENTER);
}

void draw() {
 background(0, 10, 80);
 

 int numberOfRectangles = 10;
 float step = 2.0*PI/numberOfRectangles;

 // Translate the whole coordinate system so that the middle of the window is 0,0
 translate(width/2, height/2);
 for(int i=0; i<numberOfRectangles; i++)
   {
    // this time we rotate the whole coordinate system
    // This way our rectangles will not only be drawn at the right position, but also rotated
    rotate(step);
    rect(0, 130, 60, 60);
   }
}