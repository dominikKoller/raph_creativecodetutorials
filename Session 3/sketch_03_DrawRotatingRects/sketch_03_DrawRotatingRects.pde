ArrayList<RotatingRect> rects;

void setup() {
  size(300,300);
  colorMode(HSB);
  rectMode(CENTER);
  
  stroke(140, 200, 255);
  strokeWeight(3);
  
  rects = new ArrayList<RotatingRect>();
}

void draw() {
  background(70);
  
  for(int i=0; i<rects.size(); i++){
    rects.get(i).update();
    rects.get(i).draw();
  }
}

void mousePressed(){
  if(mouseButton == LEFT){
   rects.add(new RotatingRect(random(10, 100),   // size
                              random(0.01, 0.1), // speed
                              new PVector(mouseX, mouseY))); // position
  }
  else if (mouseButton == RIGHT)
    rects.clear();
}