BallSystem system;

void setup() {
  size(300,300);
  colorMode(HSB);
  rectMode(CENTER);
  noCursor();
  
  stroke(140, 200, 255);
  strokeWeight(3);
  
  system = new BallSystem();
}

void draw() {
  background(70);
  
 if(mousePressed)
    system.spray(new PVector(mouseX, mouseY), 10, 10);
    
  system.update();
  system.draw();  
  
  ellipse(mouseX, mouseY, 20, 20);
}