import processing.vr.*;

//StarShape star = new StarShape(20);

void setup() {
  fullScreen(STEREO);
  stroke(10, 50, 255);
  strokeWeight(5);
  //cameraUp();
}

void draw() {
  translate(width/2, height/2, height/1.75); // /1.75 is not exaclty right, got only by trial and error
  background(10, 20, 60);
  
  //star.update();
  
  for(int i=0; i<10; i++) {
   rotateY(19380);
   pushMatrix();
     translate(200,0,0);
     //rotateY(PI/2);
     ellipse(0, 0, 100, 100);
     star.draw(0, 0, 100, 3);
   popMatrix();
  }
  
  translate(0, 150, 0);
  drawGrid();
}

void drawAxis() {
 pushStyle();
 strokeWeight(3);
 stroke(255,0,0);
 line(0,0,0,255,0,0);
 
 stroke(0,255,0);
 line(0,0,0,0,255,0);
 
 stroke(0,0,255);
 line(0,0,0,0,0,255);
 popStyle();
}

void drawGrid() {
 pushStyle();
 strokeWeight(3);
 stroke(200);
 
 float gridSize = 1000;
 int linesNr = 25;
 float step = gridSize / (linesNr-1);
 
 for(int i=0; i<linesNr; i++) {
   line( i*step - gridSize/2, 0, -gridSize/2, 
         i*step - gridSize/2, 0,  gridSize/2);
   line( -gridSize/2, 0, i*step - gridSize/2,
          gridSize/2, 0, i*step - gridSize/2);
 }
 popStyle();
}