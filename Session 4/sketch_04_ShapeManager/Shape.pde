abstract class Shape {

  private final PVector initialSize;
  private int age = 0;
  private int lifeTime;
  
  PVector position;
  PVector size;
  color c;

  
  Shape(PVector _position, PVector _size, int _lifeTime, color _color) {
    position    = _position;
    initialSize = _size;
    size        = _size;
    lifeTime    = _lifeTime;
    c = _color;
  }
  
  void update() {
    size = PVector.mult(initialSize, 1.0*(lifeTime-age)/lifeTime);
    age++;
  }
  
  boolean isDead() {
    return age > lifeTime; 
  }
 
  abstract void draw();
}