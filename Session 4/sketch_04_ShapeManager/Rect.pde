class Rect extends Shape {
  
  Rect(PVector position, PVector size, int lifeTime, color c) {
   super(position, size, lifeTime, c);
  }
  
  void draw(){
   pushStyle();
    fill(c);
    rect(position.x, position.y, size.x, size.y); 
   popStyle();
  }
}