// This approach does not yet use objects,
// but it shows how what we have right now becomes inconvenient:
// a circle now has not only a size and a position, but also a color and a speed
// this means we have to keep our arrays exactly aligned! 
// when we say 'the first circle', we mean the first element of every one of these arrays, which is tedious..

float[] circleSize;
PVector[] circlePosition;
color[] circleColor;
float[] circleSpeed;

int numberOfCircles = 100;

void setup() {
  size(400,400);
  background(200);
  colorMode(HSB);
  
  circleSize = new float[numberOfCircles];
  circlePosition = new PVector[numberOfCircles];
  circleColor = new color[numberOfCircles];
  circleSpeed = new float[numberOfCircles];
  
  for(int i=0; i<numberOfCircles; i++){
    circleSize[i] = random(150) + 10; // random value between 10 and 160
    circlePosition[i] = new PVector(random(width), random(height));
    circleColor[i] = color(random(255), 200, 205);
    circleSpeed[i] = random(10) + 1;
  }
}

void draw() {
  background(20);
  
  for(int i=0; i<numberOfCircles; i++){
  float currentCircleSize = sin(circleSpeed[i]*frameCount / 60) * circleSize[i];
  fill(circleColor[i]);
  ellipse(circlePosition[i].x,
          circlePosition[i].y, 
          currentCircleSize, 
          currentCircleSize);
  }
}