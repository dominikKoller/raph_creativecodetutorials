ColorStack colorStack;
color lastColor = color(255, 255, 255);

void setup() {
  size(300, 700);
  colorMode(HSB);
  textSize(30);
  noCursor();
  noStroke();
  
  colorStack = new ColorStack();
}

void draw() {
  background(20);
  
  for(int i=0; i<colorStack.size(); i++){
    fill(colorStack.get(i));
    rect(50, i*60, 40, 40);
  }
  
  fill(lastColor);
  ellipse(mouseX, mouseY, 30, 30);
}

void mousePressed(){
  if (mouseButton == LEFT)
    colorStack.push(color(random(255), 200, 255));
  else
    lastColor = colorStack.pop();
}
