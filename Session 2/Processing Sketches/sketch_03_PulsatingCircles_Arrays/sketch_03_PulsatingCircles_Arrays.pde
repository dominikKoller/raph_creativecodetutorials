// This approach uses arrays for circleSize and circlePosition
// This makes our code much more concise!
// Also we can now draw 100 circles without chaning our code at all.

float[] circleSize;
PVector[] circlePosition;

int numberOfCircles = 100;

void setup() {
  size(400,400);
  background(200);
  
  circleSize = new float[numberOfCircles];
  circlePosition = new PVector[numberOfCircles];
  
  for(int i=0; i<numberOfCircles; i++){
    circleSize[i] = random(150) + 10; // random value between 10 and 160
    circlePosition[i] = new PVector(random(width), random(height));
  }
}

void draw() {
  background(20);
  
  for(int i=0; i<numberOfCircles; i++){
  float currentCircleSize = sin(1.0*frameCount / 100) * circleSize[i];
  ellipse(circlePosition[i].x, 
          circlePosition[i].y, 
          currentCircleSize, 
          currentCircleSize);
  }
}