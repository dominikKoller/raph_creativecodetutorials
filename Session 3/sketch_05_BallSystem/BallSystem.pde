class BallSystem {
 
  ArrayList<Ball> balls;
 
  BallSystem(){
   balls = new ArrayList<Ball>(); 
  }
  
  void update() {
   for(int i=0; i<balls.size(); i++){
    balls.get(i).update();
    
    if(balls.get(i).age > 100) {
     balls.remove(i);
     i--;
    }
   }
  }
  
  void draw() {
   for(int i=0; i<balls.size(); i++){
     balls.get(i).draw();
   }
  }
  
  void spray(PVector position, int amount, float speed){
   for(int i=0; i<amount; i++){
    PVector speedV = PVector.random2D();
    speedV.setMag(speed);
    balls.add(new Ball(speedV, position.copy(), random(10, 100)));
   }
  }
}