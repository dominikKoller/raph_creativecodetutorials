class RotatingRect {
 float rotation = 0;
 float size;
 float speed;
 PVector position;
 
 RotatingRect(float _size, float _speed, PVector _position) {
   size = _size;
   speed = _speed;
   position = _position;
 }
 
 void update() {
  rotation += speed; 
 }
 
 void draw() {
  pushMatrix();
   translate(position.x, position.y);
   rotate(rotation);
   rect(0,0,size, size);
  popMatrix();
 }
}