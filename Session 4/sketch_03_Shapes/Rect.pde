class Rect extends Shape {
  
  Rect(PVector _position, PVector _size) {
   super(_position, _size);
  }
  
  void draw(){
   rect(position.x, position.y, size.x, size.y); 
  }
}