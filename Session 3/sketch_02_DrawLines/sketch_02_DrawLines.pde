ArrayList<PVector> points;

void setup() {
  size(300,300);
  colorMode(HSB);
  
  stroke(140, 200, 255);
  strokeWeight(3);
  
  points = new ArrayList<PVector>();
}

void draw() {
  background(70);
  
  for(int i=0; i<points.size()-1; i++)
    line(points.get(i).x, points.get(i).y, points.get(i+1).x, points.get(i+1).y);
    
  if(points.size() > 0) {
    PVector last = points.get(points.size()-1);
    line(last.x, last.y, mouseX, mouseY);
  }
}

void mousePressed(){
  if(mouseButton == LEFT)
    points.add(new PVector(mouseX, mouseY));
  else if (mouseButton == RIGHT)
    points.clear();
}