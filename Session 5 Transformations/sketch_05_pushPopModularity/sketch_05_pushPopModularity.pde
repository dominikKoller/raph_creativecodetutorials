// We already saw the main reason why we why we want to use push an pop:
// it makes things much more modular
// by making sure that one part of our code doesn't interfere
// with other parts of our code

// Here's an example where that is even more clear:
// It's got a mistake - there are several ways to fix it!

ArrayList<RandomCircle> circles;

void setup() {
  size(900, 200);
  colorMode(HSB);
  textSize(30);
  noCursor();
  noStroke();
  
  circles = new ArrayList<RandomCircle>();
}

void draw() {
  background(20);
  
  for(int i=0; i<circles.size(); i++)
    circles.get(i).draw();
    
  ellipse(mouseX, mouseY, 50, 50);
  text("Color of this text is the same as the last circle drawn!", 30, 50);
  text("Often, you don't want one module to effect another. Fix it!", 30, 150);
}

void mousePressed() {
  circles.add(new RandomCircle());
}

void drawRedRects(){
  
 int numberOfRects = 10;
 float space = (float)width / (float)numberOfRects;
 for(int x=0; x<10; x++)
 for(int y=0; y<10; y++)
 {
  rect(space*x, space*y, 20, 20);
 }
 
}
