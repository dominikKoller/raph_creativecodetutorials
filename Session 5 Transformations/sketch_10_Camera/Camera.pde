class TwoDCamera {
  PVector position;
  int zoom;
  
  TwoDCamera () {
    position = new PVector(0,0);   
    zoom = 1;
  }
  
  void update() {
    if (keyPressed && key == CODED){
    if (keyCode == RIGHT)
      position.x -= 1;
    else if (keyCode == LEFT)
      position.x += 1;
    else if (keyCode == DOWN)
      position.y -= 1;
    else if (keyCode == UP)
      position.y += 1;
    }
  }
  
  void apply() {
   translate(position.x, position.y);
   scale(zoom);
  }
}
