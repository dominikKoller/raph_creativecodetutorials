abstract class FollowShape {

  PVector size;
  Follower follower;
  int age = 0;
  int lifeTime;

  
  FollowShape(PVector _position, PVector _size, float _followSpeed) {
    size = _size;
    follower = new Follower(_position, _followSpeed);
  }
  
  void update() {
    follower.update();
    age++;
  }
  
  PVector getPosition() {
    return follower.position;
  }
  
  void setGoal(PVector goal) {
    follower.goal = goal; 
  }
  
  abstract void draw();
}