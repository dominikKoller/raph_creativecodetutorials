void setup() {
 size(1500, 300);
 smooth(4);
  
 stroke(230);
 strokeWeight(5);
 fill(50, 200, 255);
 rectMode(CENTER);
}

void draw() {
 background(0, 10, 80);
 
 int numberOfRectangles = 10;
 float step = 1.0*width/numberOfRectangles;
 for(int i=0; i<10; i++)
   {
    rect(step/2 + step*i, height/2, 100, 100);
   }
}