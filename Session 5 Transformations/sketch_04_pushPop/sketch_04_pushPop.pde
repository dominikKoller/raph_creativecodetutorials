// We already saw the main reason why we why we want to use push an pop:
// it makes things much more modular
// by making sure that one part of our code doesn't interfere
// with other parts of our code

// Here's an example where that is even more clear:

void setup() {
  size(250, 300);
  colorMode(HSB);
  
  noStroke();
  fill(100, 200, 255);
}

void draw() {
  background(20);
  
  fill(0, 20, 200); // Grey
  rect(10, 10, 40, 40);
  pushStyle();
    fill(100, 200, 155); // Green
    rect(10, 60, 40, 40);
    pushStyle();
      fill(0, 200, 255); // Red
      rect(10, 110, 40, 40);
    popStyle(); // Back to Green
    rect(10, 160, 40, 40);
  popStyle(); // Back to Grey
  rect(10, 210, 40, 40);
}
