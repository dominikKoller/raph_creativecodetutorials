void setup() {
 size(500, 500);
 smooth(4);
  
 stroke(150);
 strokeWeight(5);
 fill(50, 200, 255);
}

void draw() {
 background(0, 10, 80);
 
 // Making the number of circles dependant on the mouseY position (but at least 3)
 int numberOfCircles = 5;
 
 // This will be the vector we rotate for every circle we draw
 PVector v = new PVector(0, 60);
 float step = 2*PI/numberOfCircles;
 for(int i=0; i<numberOfCircles; i++)
   {
    v.rotate(step); // change the variable v
    PVector mouse = new PVector(mouseX, mouseY);
    PVector pos = PVector.add(mouse, v);
    ellipse(pos.x, pos.y, 60, 60);
   }
}