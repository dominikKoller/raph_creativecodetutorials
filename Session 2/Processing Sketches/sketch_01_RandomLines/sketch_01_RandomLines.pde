void setup() {
  size(400,400);
  background(200);
}

void draw() {
  drawRandomLine();
}

// our own function, called drawRandomLine
void drawRandomLine(){
 stroke(random(255));
 line(random(width), random(height), random(width), random(height));
}

// Reminder: a function has a return value that we can use like any other value 
// That means we can use it as a paramter for another function or store it in a variable, like that:
// float myRandomNumber = random(100); 

// a 'void' function has no return value. It might still _do_ something for us, like drawing a line to the screen.