// The problem we had was that we changed the state of the whole program
// when all we wanted to do was draw a different colored ellipse when the mouse if pressed!

// To fix this, we want to remember the old state (using pushStyle)
// and then restore it (using popStyle)

// Also note: 
// I'm using the word 'state' to refer to the current fill color,
// but the 'state' is much more general. In processing, 'style' refers to
// all drawing-style state, like fill color, stroke color, stroke width, color mode, etc
// while 'matrix' refers to the current transformation, like translate, rotate, shear, etc.

void setup() {
  size(700, 300);
  colorMode(HSB);
  noCursor(); // don't show the mouse cursor inside window
  
  
  noStroke();
  fill(100, 200, 255);
}

void draw() {
  background(20);
  ellipse(mouseX, mouseY, 50, 50);
  
  textSize(35);
  text("This text will now always be green.", 50, 100);
  textSize(25);
  text("Don't leave the state changed if you don't intend to!", 50, 150);
  
  if(mousePressed) {
   pushStyle(); // remember old state
     fill(245, 200, 255);
     ellipse(mouseX, mouseY, 100, 100);
   popStyle(); // restore old state
  }
}
