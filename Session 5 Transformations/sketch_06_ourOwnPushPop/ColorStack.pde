class ColorStack {
  
  // This implementation is not at all made for efficiency
  // rather, it's using concepts we know already
  // to recreate the behaviour of a 'stack' datatype
  
  ArrayList<Integer> stack;
  
  ColorStack(){
    stack = new ArrayList<Integer>();
  }
  
  void push(color _color){
    stack.add(0, _color); 
  }
  
  color pop() {
    return stack.remove(0); 
  }
  
  int size() {
    return stack.size();
  }
  
  // A stack does not usually have this operation!
  // It cannot, for simplicity and performance reasons.
  // We have it here only to illustrate 
  color get(int i) {
    return stack.get(i);
  }
}
