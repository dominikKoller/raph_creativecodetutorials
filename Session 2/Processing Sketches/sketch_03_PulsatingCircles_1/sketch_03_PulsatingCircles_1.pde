// This is an approach that uses neither Arrays nor Objects.
// Because of that, it is very bulky:
// We have to keep track of a lot of variables and copy a lot of code

float circleSize1 = 200;
float circleSize2 = 100;
float circleSize3 = 30;

PVector circlePosition1;
PVector circlePosition2;
PVector circlePosition3;

void setup() {
  size(400,400);
  background(200);
  
  circlePosition1 = new PVector(random(width), random(height));
  circlePosition2 = new PVector(random(width), random(height));
  circlePosition3 = new PVector(random(width), random(height));
}

void draw() {
  background(20);
  
  float currentCircleSize1 = sin(1.0*frameCount / 100) * circleSize1;
  ellipse(circlePosition1.x, 
          circlePosition1.x, 
          currentCircleSize1, 
          currentCircleSize1);
  
  float currentCircleSize2 = sin(1.0*frameCount / 100) * circleSize2;
  ellipse(circlePosition2.x, 
          circlePosition2.x, 
          currentCircleSize2, 
          currentCircleSize2);
  
  float currentCircleSize3 = sin(1.0*frameCount / 100) * circleSize3;
  ellipse(circlePosition3.x, 
          circlePosition3.x, 
          currentCircleSize3, 
          currentCircleSize3);
}