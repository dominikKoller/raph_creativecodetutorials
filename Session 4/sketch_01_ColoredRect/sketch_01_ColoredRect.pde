Rect rect;
ColoredRect colRect;

void setup() {
  size(500, 300);
  rectMode(CENTER);
  colorMode(HSB);
  strokeWeight(5);
  
  rect = new Rect(new PVector(width/2, height/2), // position
                  new PVector(300, 200) );        // size
               
  colRect = new ColoredRect(new PVector(width/2, height/2), // position
                            new PVector(100, 100),          // size 
                            color(200, 200, 200),           // fill Color
                            color(100, 200, 200));          // stroke Color
}

void draw() {
  background(20);
  
  // If the mouse is pressed, we'll use a random fill color
  // This will effect the rect, but not the colRect!
  // Since the colRect changes to its own color in draw()
  
  if(mousePressed)
    fill(random(255), 200, 200);
    
  rect.draw();
  colRect.draw();
}