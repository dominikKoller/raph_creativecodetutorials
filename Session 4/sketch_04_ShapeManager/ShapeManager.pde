class ShapeManager {
  ArrayList<Shape> shapes;
  
  ShapeManager() {
    shapes = new ArrayList<Shape>(); 
  }
  
  void update(){
    for(int i=0; i<shapes.size(); i++) {
      shapes.get(i).update();
      if(shapes.get(i).isDead()) {
        shapes.remove(i);
        i--;
      }
    }
  }
  
  void draw(){
    for(int i=0; i<shapes.size(); i++) {
      shapes.get(i).draw(); 
    }
  }
  
  // Selects randomly from an Ellipse, Rect or Star
  // at the given position
  // and adds it to the shapes
  void addAt(PVector p) {
    float r = random(3);
    
    PVector size = new PVector(20, 20);
    int lifeTime = ceil(random(50, 200));
    color c = color(random(255), 200, 200);
    
    if(r < 1)
      shapes.add(new Rect(p, size, lifeTime, c));
    
    else if(r < 2)
      shapes.add(new Ellipse(p, size, lifeTime, c));
    
    else
      shapes.add(new Star(p, size, lifeTime, ceil(random(3, 6)), c));
  }
}