class FollowEllipse extends FollowShape {
  
  FollowEllipse(PVector _position, PVector _size, float _followSpeed) {
   super(_position, _size, _followSpeed);
  }
  
  void draw(){
   ellipse(getPosition().x, getPosition().y, size.x, size.y); 
  }
}