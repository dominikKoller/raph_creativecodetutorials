// In this sketch, we look at what mistakes we can make
// when forgetting about a global state

// Can you figure out what's happening?
// Do you remember how it's fixed?
// (try to figure it out before looking at the next sketch!)

void setup() {
  size(700, 300);
  colorMode(HSB);
  noCursor(); // don't show the mouse cursor inside window
  
  
  noStroke();
  fill(100, 200, 255);
}

void draw() {
  background(20);
  ellipse(mouseX, mouseY, 100, 100);
  
  textSize(35);
  text("This text should always be green!", 50, 100);
  textSize(25);
  text("..but press the mouse and see what happens..", 50, 150);
  
  if(mousePressed) {
   fill(245, 200, 255);
   ellipse(mouseX, mouseY, 150, 150);
  }
}
