class FollowTheMouse {
  ArrayList<FollowShape> shapes;
  int lifeTime = 100;
  
  FollowTheMouse(){
    shapes = new ArrayList<FollowShape>();
  }
  
  void update(PVector goal){
    for(int i=0; i<shapes.size(); i++) {
      shapes.get(i).setGoal(goal);
      shapes.get(i).update();
      if(shapes.get(i).age > lifeTime) {
        shapes.remove(i);
        i--;
      }
    }
  }
  
  void draw(){
    for(int i=0; i<shapes.size(); i++) {
      shapes.get(i).draw(); 
    }
  }
  
  void explodeAt(PVector p) {
    
  }
  
  void addAt(PVector p) {
    float r = random(3);
    
    PVector size = new PVector(20, 20);
    float followSpeed = random(0.1, 0.2);
    
    if(r < 1)
      shapes.add(new FollowRect(p, size, followSpeed));
    
    else if(r < 2)
      shapes.add(new FollowEllipse(p, size, followSpeed));
    
    else
      shapes.add(new FollowStar(p, size, followSpeed, ceil(random(3, 6))));
  }
}