void setup() {
  size(400,400);
  colorMode(HSB);
  fill(200,200,200);
}

void draw() {
  background(20);
  
  // instead of 
  // rect (210, 210, 100, 100);
  // We can do:
  
  translate(200, 100);
  // then we could also rotate!
  // rotate(mouseX / 100.0);
  
  drawGrid();
  rect(10,10, 100, 100);
  
}

void drawGrid() {
 pushStyle();
   fill(255, 220);
   noStroke();
   rect(0,0,width,height);
 
   stroke(0, 90);

   int lineCount = 10;
   float spacing = (float) max(width-1, height-1) / (float) lineCount;
   for(int x = 0; x<=lineCount; x++)
   {
     line(x*spacing, 0, x*spacing, height);
   }
   for(int y = 0; y<=lineCount; y++)
   {
     line(0, y*spacing, width, y*spacing);
   }
 popStyle();
}
