FollowTheMouse tail;

void setup() {
  size(500, 300);
  rectMode(CENTER);
  colorMode(HSB);
  strokeWeight(5);
  
  tail = new FollowTheMouse();
}

void draw() {
  println(frameCount);
  background(50);
  
  // If the mouse is pressed, we'll use a random fill color
  // This will effect the rect, but not the colRect!
  // Since the colRect changes to its own color in draw()
  if(mousePressed)
    tail.addAt(new PVector(mouseX, mouseY));
    
  tail.update(new PVector(mouseX, mouseY));
  tail.draw();
}

void mousePressed() {

}