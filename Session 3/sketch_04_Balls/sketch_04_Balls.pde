ArrayList<Ball> balls;

void setup() {
  size(300,300);
  colorMode(HSB);
  rectMode(CENTER);
  
  stroke(140, 200, 255);
  strokeWeight(3);
  
  balls = new ArrayList<Ball>();
}

void draw() {
  background(70);
  
  for(int i=0; i<balls.size(); i++){
    balls.get(i).applyForce(new PVector(0, 0.3));
    balls.get(i).update();
    balls.get(i).draw();
  }
 
}

void mousePressed(){
  if(mouseButton == LEFT){
   balls.add(new Ball(new PVector(random(-5, 5), random(-5, 5)), // speed
                          new PVector(mouseX, mouseY),
                          random(10, 100))); // size
  }
  
  else if (mouseButton == RIGHT)
    balls.clear();

}