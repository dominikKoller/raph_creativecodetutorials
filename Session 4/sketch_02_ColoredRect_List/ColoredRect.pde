class ColoredRect extends Rect {
  
  color fillCol;
  color strokeCol;
  
  ColoredRect(PVector _position, PVector _size, color _fillCol, color _strokeCol) {
    super(_position, _size);
    fillCol = _fillCol;
    strokeCol = _strokeCol;
  }
  
  void draw() {
    pushStyle();
     fill(fillCol);
     stroke(strokeCol);
     super.draw();
    popStyle();
  }
}