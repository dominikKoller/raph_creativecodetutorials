void drawGrid() {
 pushStyle();
   fill(255, 220);
   noStroke();
   rect(0,0,width,height);
 
   stroke(0, 90);

   int lineCount = 10;
   float spacing = (float) max(width-1, height-1) / (float) lineCount;
   for(int x = 0; x<=lineCount; x++)
   {
     line(x*spacing, 0, x*spacing, height);
   }
   for(int y = 0; y<=lineCount; y++)
   {
     line(0, y*spacing, width, y*spacing);
   }
 popStyle();
}
