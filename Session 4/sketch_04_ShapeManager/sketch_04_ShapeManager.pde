ShapeManager manager;

void setup() {
  size(500, 300);
  rectMode(CENTER);
  colorMode(HSB);
  strokeWeight(4);
  smooth();
  
  manager = new ShapeManager();
}

void draw() {
  println(frameCount);
  background(50);
  

    
  manager.update();
  manager.draw();
}

void mouseMoved() {
  manager.addAt(new PVector(mouseX, mouseY));
}