 //This approach does not yet use objects,
// but it shows how what we have right now becomes inconvenient:
// a circle now has not only a size and a position, but also a color and a speed
// this means we have to keep our arrays exactly aligned! 
// when we say 'the first circle', we mean the first element of every one of these arrays, which is tedious..

PulsatingCircle[] circle;
int numberOfCircles = 100;
void setup() {
  size(400,400);
  background(200);
  colorMode(HSB);
  
  circle = new PulsatingCircle[numberOfCircles];
  for(int i=0; i<numberOfCircles; i++){
    circle[i] = new PulsatingCircle(random(150) + 10,                           // size
                                    new PVector(random(width), random(height)), // position
                                    color(random(255), 200, 205),               // color
                                    random(10) + 1);                            // speed
  }
}

void draw() {
  background(20);
  
  for(int i=0; i<numberOfCircles; i++){
    circle[i].draw();
    println(circle[i].size); // WHY DOES THIS WORK WTF
  }
}