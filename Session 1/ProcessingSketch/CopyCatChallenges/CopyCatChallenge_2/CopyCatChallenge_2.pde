void setup() {
 size(500, 500);
 smooth(4);
  
 stroke(150);
 strokeWeight(5);
 fill(50, 200, 255);
 
 rectMode(CENTER);
}

void draw() {
 background(0, 10, 80);
 
 // The point at the middle of the window
 PVector middle = new PVector(width/2, height/2);
 
 int numberOfCircles = 10;
 // This will be the vector we rotate for every circle we draw
 PVector v = new PVector(0, 130);
 float step = 2.0*PI/numberOfCircles;
 
 // Here is a solution that uses only things we've already seen:
 
 // Drawing half the circles, but with double the distance between them (step * 2)
 for(int i=0; i<numberOfCircles / 2; i++)
   {
    v.rotate(step * 2); // change the variable v
    PVector pos = PVector.add(middle, v);
    ellipse(pos.x, pos.y, 60, 60);
   }
   
 // Resetting v, so starting anew
 v = new PVector(0, 130);
 // But with an offset of exactly one step
 v.rotate(step);
   
 // Now drawing all the Rectangles, again with step * 2 in between them, but this time we have the offset by one step
 for(int i=0; i<=numberOfCircles / 2; i++)
   {
    v.rotate(step * 2); // change the variable v
    PVector pos = PVector.add(middle, v);
    rect(pos.x, pos.y, 60, 60);
   }
   
   
 // Here's a more elegant solution, that simply checks whether the current iteration count (i) is even or odd:
 /*
 for(int i=0; i<numberOfCircles; i++)
   {
    v.rotate(step); // change the variable v
    PVector pos = PVector.add(middle, v);
    if(isEven(i))
      ellipse(pos.x, pos.y, 60, 60);
    else
      rect(pos.x, pos.y, 50, 50);
   }
   */
}

// To find out why this works, look at 
// https://forum.processing.org/two/discussion/10398/how-to-test-if-a-number-is-odd-or-even
// https://stackoverflow.com/questions/17524673/understanding-the-modulus-operator
boolean isEven(int n){
  return n % 2 == 0;
}