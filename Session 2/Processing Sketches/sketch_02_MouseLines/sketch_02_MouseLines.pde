void setup() {
  size(400,400);
  background(200);
  
  fill(0,10); // black with transparency
}

void draw() {
  noStroke();
  rect(0,0,width,height); // draw over the whole screen, but with transparent fill
  
  for(int i=0; i<10; i++) {
   stroke(random(255));
   line(random(width), random(height), mouseX, mouseY);
  }
}