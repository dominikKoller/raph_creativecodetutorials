float circleSize = 200;

void setup() {
  size(400,400);
  background(200);
}

void draw() {
  background(20);
  
  float currentCircleSize = sin(1.0*frameCount / 100) * circleSize;
  ellipse(width/2, height/2, currentCircleSize, currentCircleSize);
}