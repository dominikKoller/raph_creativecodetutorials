// This sketch is all about _state_

// There are variables built into processing
// Like the current fill color, stroke color, stroke width etc

// What functions like fill(..) ad stroke(..) do is 
// they _change that state_

// This has no immediate effect on the screen,
// but will effect whatever you do after that -
// like drawing a circle in a particular color

void setup() {
  size(300, 300);
  colorMode(HSB);
  noCursor(); // don't show the mouse cursor inside window
  
  noStroke();
  fill(100, 200, 255);
}

void draw() {
  background(20);
  ellipse(mouseX, mouseY, 100, 100);
}

void mousePressed() {
 fill(245, 200, 255);
 // once you pressed the mouse, the circle will now
 // always be red (until you call fill with some other color of course!)
}
