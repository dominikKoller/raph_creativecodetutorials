ArrayList<Shape> shapes;

void setup() {
  size(500, 300);
  rectMode(CENTER);
  colorMode(HSB);
  strokeWeight(5);
  
  shapes = new ArrayList<Shape>();
}

void draw() {
  println(frameCount);
  background(50);
  
  // If the mouse is pressed, we'll use a random fill color
  // This will effect the rect, but not the colRect!
  // Since the colRect changes to its own color in draw()
  
  for(Shape s : shapes)
    s.draw(); // This time, they are all Shapes! Some are Rects, some are Ellipses.
}

void mousePressed() {
    if(mouseButton == LEFT) {
    shapes.add(  new Rect(new PVector(mouseX, mouseY),  // position
                          new PVector(40, 40) ));       // size
  }
  
  else if (mouseButton == RIGHT) {
    shapes.add(  new Ellipse(new PVector(mouseX, mouseY),  // position
                             new PVector(40, 40)));        // size 
  }
  
  else if (mouseButton == CENTER) {
    shapes.add(  new Star(new PVector(mouseX, mouseY), // position
                          new PVector(40, 40),         // size
                          ceil(random(5, 10))));      // number of points
  }
}