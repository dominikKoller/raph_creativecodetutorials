TwoDCamera camera;

void setup() {
  size(400, 400);
  colorMode(HSB);
  noStroke();
  fill(150, 200, 100);
  textSize(20);

  camera = new TwoDCamera();
}

void draw() {
  background(20);

  camera.update();
  camera.apply();
  drawGrid();

textSize(20);
  text("Use Arrow Keys to move the camera!", 30, 60);
  textSize(12);
  text("Try making more controls like zoom, using the mouse, etc", 30, 100);
}
