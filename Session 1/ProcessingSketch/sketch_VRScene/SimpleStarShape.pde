class SimpleStarShape {
 
  int FSpikeCount;
  PVector[] FVertices;
  
  SimpleStarShape(int spikeCount, float randomIntensity) {
    FSpikeCount = spikeCount;
    FVertices = new PVector[FSpikeCount];
    
    float rotationStep = 2*PI / FSpikeCount;
    for(int i=0; i<FSpikeCount; i++) {
      FVertices[i] = PVector.fromAngle(rotationStep*i);
    }
    
    resetRandom(randomIntensity);
  }
  
  public void draw(float x, float y, float size) {   
    
    beginShape();
    for(int i=0; i<FSpikeCount; i++)
      vertex(x + FVertices[i].x * size/2.0, y + FVertices[i].y * size/2.0);
    endShape(CLOSE);
  }
    
  private void resetRandom(float randomIntensity) {
  for(int i=0; i<FSpikeCount; i++)
      FVertices[i].setMag(1.0 + random(randomIntensity) - randomIntensity/2.0);
  }
}