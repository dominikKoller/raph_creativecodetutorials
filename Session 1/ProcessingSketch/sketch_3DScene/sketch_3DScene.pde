import peasy.PeasyCam;

PeasyCam camera;
//StarShape star = new StarShape(20);

void setup() {
  size(500,500, P3D);
  camera = new PeasyCam(this, 400);
}

void draw() {
  background(10, 20, 60);
  drawAxis();
  //star.update();
  
  for(int i=0; i<10; i++) {
   rotateY(19380);
   pushMatrix();
     translate(200,0,0);
     rotateY(PI/2);
     ellipse(0, 0, 100, 100);
     //star.draw(0, 0, 100, 3);
   popMatrix();
  }
}

void drawAxis() {
 strokeWeight(3);
 stroke(255,0,0);
 line(0,0,0,255,0,0);
 
 stroke(0,255,0);
 line(0,0,0,0,255,0);
 
 stroke(0,0,255);
 line(0,0,0,0,0,255);
}