class FollowRect extends FollowShape {
  
  FollowRect(PVector _position, PVector _size, float _followSpeed) {
   super(_position, _size, _followSpeed);
  }
  
  void draw(){
   rect(getPosition().x, getPosition().y, size.x, size.y); 
  }
}