class Ball {
 PVector speed;
 PVector position;
 float size;
 int age;
 
 Ball (PVector _speed, PVector _position, float _size) {
   speed = _speed;
   position = _position;
   size = _size;
 }
 
 void applyForce(PVector force){
   this.speed.add(force);
 }
 
 void update() {
  this.position.add(this.speed);
  age += 1;
 }
 
 void draw() {
  pushMatrix();
   translate(this.position.x, this.position.y);
   ellipse(0,0,size, size);
  popMatrix();
 }
}