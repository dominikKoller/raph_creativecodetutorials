class Mouse {
 
  PVector position;
  float rotation;
  float size;
  
  Mouse (PVector _position, float _rotation, float _size) {
    position = _position;
    rotation = _rotation;
    size     = _size;
  }
  
  void draw() {
  pushMatrix();
  pushStyle();
  
  translate(position.x, position.y);
  scale(size / 200, size / 200); // I was lazy there, the mice were too big but I didn't want to change the drawing code
  rotate(rotation);
  
    // amazingly drawn mouse:
  noStroke();
  fill(100, 240, 255);
  ellipse(40, -40, 80, 80);
  ellipse(-40, -40, 80, 80);
  
  fill(150, 200, 200);
  ellipse(0,0, 100, 100);
  
  fill(100, 10, 255);
  ellipse(15, -20, 35, 35);
  ellipse(-15, -20, 35, 35);
  
  fill(245, 200, 255);
  ellipse(0, -5, 20, 20);
  
  popMatrix();
  popStyle();
  }
}
